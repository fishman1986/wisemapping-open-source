<map version="0.9.0">
    <node ID="ID_1" TEXT="SaberM&#225;s">
        <node ID="ID_5" POSITION="right" TEXT="Utilizaci&#243;n de medios de expresi&#243;n art&#237;stica, digitales y anal&#243;gicos"/>
        <node ID="ID_9" POSITION="left" TEXT="Precio tambi&#233;n limitado: 100-120?"/>
        <node ID="ID_2" POSITION="right" TEXT="Talleres tem&#225;ticos">
            <node ID="ID_13" POSITION="right" TEXT="Naturaleza">
                <node ID="ID_17" POSITION="right" TEXT="Animales, Plantas, Piedras"/>
            </node>
            <node ID="ID_21" POSITION="right" TEXT="Arqueolog&#237;a"/>
            <node ID="ID_18" POSITION="right" TEXT="Energ&#237;a"/>
            <node ID="ID_16" POSITION="right" TEXT="Astronom&#237;a"/>
            <node ID="ID_20" POSITION="right" TEXT="Arquitectura"/>
            <node ID="ID_11" POSITION="right" TEXT="Cocina"/>
            <node ID="ID_24" POSITION="right" TEXT="Poes&#237;a"/>
            <node ID="ID_25" POSITION="right" TEXT="Culturas Antiguas">
                <node ID="ID_26" POSITION="right" TEXT="Egipto, Grecia, China..."/>
            </node>
            <node ID="ID_38" POSITION="right" TEXT="Paleontolog&#237;a"/>
        </node>
        <node ID="ID_6" POSITION="left" TEXT="Duraci&#243;n limitada: 5-6 semanas"/>
        <node ID="ID_7" POSITION="left" TEXT="Ni&#241;os y ni&#241;as que quieren saber m&#225;s"/>
        <node ID="ID_8" POSITION="left" TEXT="Alternativa a otras actividades de ocio"/>
        <node ID="ID_23" POSITION="right" TEXT="Uso de la tecnolog&#237;a durante todo el proceso de aprendizaje"/>
        <node ID="ID_3" POSITION="right" TEXT="Estructura PBL: aprendemos cuando buscamos respuestas a nuestras propias preguntas "/>
        <node ID="ID_4" POSITION="right" TEXT="Trabajo basado en la experimentaci&#243;n y en la investigaci&#243;n"/>
        <node ID="ID_10" POSITION="left" TEXT="De 8 a 12 a&#241;os, sin separaci&#243;n por edades"/>
        <node ID="ID_19" POSITION="left" TEXT="M&#225;ximo 10/1 por taller"/>
        <node ID="ID_37" POSITION="right" TEXT="Actividades centradas en el contexto cercano"/>
        <node ID="ID_22" POSITION="right" TEXT="Flexibilidad en el uso de las lenguas de trabajo (ingl&#233;s, castellano, esukara?)"/>
        <node ID="ID_27" POSITION="right" STYLE="bubble" TEXT="Complementamos el trabajo de la escuela">
            <richcontent TYPE="NOTE">
                <html>
                    <head/>
                    <body>
                        <p>Todos los contenidos de los talleres est&#225;n relacionados con el curr&#237;culo de la ense&#241;anza b&#225;sica.</p>
                        <p>A diferencia de la pr&#225;ctica tradicional, pretendemos ahondar en el conocimiento partiendo de lo que realmente interesa al ni&#241;o o ni&#241;a,</p>
                        <p>ayud&#225;ndole a que encuentre respuesta a las preguntas que &#233;l o ella se plantea.</p>
                        <p/>
                        <p>Por ese motivo, SaberM&#225;s proyecta estar al lado de los ni&#241;os que necesitan una motivaci&#243;n extra para entender la escuela y fluir en ella,</p>
                        <p>y tambi&#233;n al lado de aquellos a quienes la curiosidad y las ganas de saber les lleva m&#225;s all&#225;.</p>
                    </body>
                </html>
            </richcontent>
            <node ID="ID_30" POSITION="right" TEXT="Cada uno va a su ritmo, y cada cual pone sus l&#237;mites"/>
            <node ID="ID_31" POSITION="right" TEXT="Aprendemos todos de todos"/>
            <node ID="ID_33" POSITION="right" TEXT="Valoramos lo que hemos aprendido"/>
            <node ID="ID_28" POSITION="right" TEXT="SaberM&#225;s trabaja con, desde y para la motivaci&#243;n"/>
            <node ID="ID_32" POSITION="right" TEXT="Trabajamos en equipo en nuestros proyectos "/>
        </node>
    </node>
</map>