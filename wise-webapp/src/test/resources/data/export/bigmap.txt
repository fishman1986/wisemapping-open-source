1 corona
	1.1 Modelo in world
		1.1.1 International market protected Modelo from unstable peso
		1.1.2 Fifth largest distributor in world
			1.1.2.1 Can they sustain that trend
			1.1.2.2 in 12 years
		1.1.3 One of top 10 breweries in world
	1.2 Carloz Fernandez CEO
		1.2.1 CEO Since 1997
			1.2.1.1 29 years old
				1.2.1.1.1 working there since 13
		1.2.2 vision: top five brewers
			1.2.2.1 International Business model
				1.2.2.1.1 experienced local distributors
				1.2.2.1.2 Growing international demand
				1.2.2.1.3 Capitalize on NAFTA
			1.2.2.2 top 10 beer producers in world
				1.2.2.2.1 7.8 % sales growth compounded over ten years
				1.2.2.2.2 2005
					1.2.2.2.2.1 12.3 % exports
					1.2.2.2.2.2 4% increase domestically
					1.2.2.2.2.3 export sales 30%
				1.2.2.2.3 Corona Extra
					1.2.2.2.3.1 worlds fourth best selling beer
					1.2.2.2.3.2 56% shar of domestic market
					1.2.2.2.3.3 Since 1997 #1 import in US
						1.2.2.2.3.3.1 outsold competitor by 50%
		1.2.3 Expanding production 
			1.2.3.1 renovate facility in Zacatecas
			1.2.3.2 300 million investment
	1.3 US Beer Market
		1.3.1 2nd largest nest to China
		1.3.2 Consumption six times higher per cap
		1.3.3 Groth expectations reduced
		1.3.4 80% of market
			1.3.4.1 AB
				1.3.4.1.1 75% of industry profits
			1.3.4.2 adolf coors
			1.3.4.3 Miller
		1.3.5 dense network of regional craft brewing
		1.3.6 volume main driver
	1.4 Modelo in Mexico
		1.4.1 History to 1970
			1.4.1.1 formed in 1922
				1.4.1.1.1 Pablo Diez Fernandez, Braulio Irare, Marin Oyamburr
				1.4.1.1.2 Iriarte died in 1932
				1.4.1.1.3 Diez sole owner 1936
				1.4.1.1.4 Fernandez Family Sole owner since 1936
			1.4.1.2 focus on Mexico City
			1.4.1.3 Modelo 1st Brand
			1.4.1.4 Corona 2nd Brand
				1.4.1.4.1 Clear Glass Customers preference
			1.4.1.5 1940s period of strong growth 
				1.4.1.5.1 concentrate domesti¬cally 
				1.4.1.5.2 improve distribution methods and produc¬tion facilities 
					1.4.1.5.2.1 distribution: direct with profit sharing
			1.4.1.6 bought the brands and assets of the Toluca y Mexico Brewery
				1.4.1.6.1 1935
				1.4.1.6.2 country's oldest brand of beer
		1.4.2 1971, Antonino Fernandez was appointed CEO
			1.4.2.1 Mexican Stock exchange in 1994
			1.4.2.2 Anheuser-Busch 17.7 % of the equity
				1.4.2.2.1 The 50.2 % represented 43.9%  voting
		1.4.3 Largest Beer producer and distrubutor in Mexico
			1.4.3.1 corona 56% share
	1.5 Modelo in US
		1.5.1 History
			1.5.1.1 1979
			1.5.1.2 Amalgamated Distillery Products Inc. (
				1.5.1.2.1 later renamed Barton Beers Ltd.
			1.5.1.3 gained popularity in southern states
			1.5.1.4 rapid growth 1980s
				1.5.1.4.1 second most popular imported beer
			1.5.1.5 1991
				1.5.1.5.1 doubling of federal excise tax on beer
					1.5.1.5.1.1 sales decrease of 15 percent
					1.5.1.5.1.2 distributor absorb the tax 92
				1.5.1.5.2 distributors took the loss
		1.5.2 2007 5 beers to us
			1.5.2.1 3 of top 8 beers in US
			1.5.2.2 Heineken
				1.5.2.2.1 Main Import Comptitor
			1.5.2.3 131 million cases
		1.5.3 Marketing
			1.5.3.1 surfing mythology
			1.5.3.2 not selling premium quality
			1.5.3.3 not testosterone driven
			1.5.3.4 found new following
			1.5.3.5 beer for non beer drinkers
			1.5.3.6 dependable second choise
			1.5.3.7 Fun in the sun
				1.5.3.7.1 Barton Beer's idea
				1.5.3.7.2 escape
				1.5.3.7.3 relaxation
			1.5.3.8 1996ad budget
				1.5.3.8.1 Corona 5.1 mil
				1.5.3.8.2 Heiniken 15 mil
				1.5.3.8.3 an bsch 192 mil
		1.5.4 Us dist contracts
			1.5.4.1 importer/distributors
				1.5.4.1.1 Local Companies
				1.5.4.1.2 Autonomous
				1.5.4.1.3 competitive relationship
				1.5.4.1.4 transportation
				1.5.4.1.5 insurance
				1.5.4.1.6 pricing
				1.5.4.1.7 customs
				1.5.4.1.8 advertixing
			1.5.4.2 procermex inc
				1.5.4.2.1 Modelo us subsidiary
				1.5.4.2.2 Support
				1.5.4.2.3 Supervise
				1.5.4.2.4 Coordinate
			1.5.4.3 Modelo had final say on brand image
			1.5.4.4 production in Mexico
			1.5.4.5 Chicago based Barton Beers 1st
				1.5.4.5.1 largest importer in 25 western states
			1.5.4.6 Gambrinus
				1.5.4.6.1 1986
				1.5.4.6.2 eastern dist
	1.6 The Beer market
		1.6.1 traditionally a clustered market
		1.6.2 many local breweries
		1.6.3 no means of transport
		1.6.4 colsolition happened in 1800s
		1.6.5 different countries had different tastes
		1.6.6 90s national leaders expanded abroad
		1.6.7 startup costs high
			1.6.7.1 industry supported conectration
		1.6.8 Interbrew
			1.6.8.1 Belgian
			1.6.8.2 aquired breweries in 20 countries
			1.6.8.3 sales in 110 countries
			1.6.8.4 local managers controlling brands
			1.6.8.5 flagship brand: Stella Artois
		1.6.9 2004 merger
			1.6.9.1 #1 Interbrew
			1.6.9.2 #5 Am Bev - Brazil
			1.6.9.3 largest merge
				1.6.9.3.1 worth 12.8 billion
		1.6.10 2007
			1.6.10.1 inbev
			1.6.10.2 SAP Miller
			1.6.10.3 Heineken
				1.6.10.3.1 produces beer domestically
					1.6.10.3.1.1 parent of local distributors
						1.6.10.3.1.1.1 marketing
						1.6.10.3.1.1.2 importing
							1.6.10.3.1.1.2.1 import taxes passed on to consumer
						1.6.10.3.1.1.3 distribution
				1.6.10.3.2 marketing
					1.6.10.3.2.1 premium beer
					1.6.10.3.2.2 premium brand
					1.6.10.3.2.3 no mythology
					1.6.10.3.2.4 superior taste
					1.6.10.3.2.5 2006 aggressive marketing campaign
						1.6.10.3.2.5.1 Heineken Premium Light
				1.6.10.3.3 reputation of top selling beer in world
				1.6.10.3.4 Dutch
			1.6.10.4 Anh Bush
				1.6.10.4.1 produces in foreign markets
		1.6.11 Beer Marketing
			1.6.11.1 People drink marketing
		1.6.12 Future
			1.6.12.1 domestic and foreign threats
			1.6.12.2 other merger talks
			1.6.12.3 Inbev in talks with Anh Bush
				1.6.12.3.1 Two biggest companies will create huge company
			1.6.12.4 Sales were decreasing due to competitive media budgets
	1.7 Mexico Industry
		1.7.1 has most trade agreements in world
		1.7.2 one of the largest domestic beer markets
		1.7.3 imported beer only 1% sales
			1.7.3.1 half were anh bcsh dist by modelo
		1.7.4 modelo
			1.7.4.1 NAFTA S.A. An Bucsh
			1.7.4.2 62.8% of market
		1.7.5 FEMSA
			1.7.5.1 domestic market
				1.7.5.1.1 37% of domestic market
				1.7.5.1.2 production and distribution in Mexico: peso not a threat
				1.7.5.1.3 Owns Oxxo C
					1.7.5.1.3.1 CA largest chain of conv stores
				1.7.5.1.4 leads domestic premium beer market
				1.7.5.1.5 997 to 2004 taking domestic market share
				1.7.5.1.6 NAFTA SACoca cola
					1.7.5.1.6.1 Exclusive distributor
			1.7.5.2 foriegn market
				1.7.5.2.1 Partnership Heiniken
					1.7.5.2.1.1 Distribution in US
				1.7.5.2.2 90s entry to us market failed
				1.7.5.2.3 Recently partnered with Heiniken for US market
					1.7.5.2.3.1 2005 18.7% growth
