<map version="0.9.0">
    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_1" TEXT="As&#237; que quieres recorrer Medell&#237;n...">
        <font SIZE="24"/>
        <edge COLOR="#ffffff"/>
        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_134" POSITION="right" STYLE="bubble" TEXT="Y ERES UN TURISTA">
            <font BOLD="true"/>
            <edge COLOR="#000000"/>
        </node>
        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_4" POSITION="left" STYLE="bubble" TEXT="Y ERES UN RESIDENTE">
            <font BOLD="true"/>
            <edge COLOR="#000000"/>
            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_6" POSITION="left" STYLE="bubble" TEXT="Divertirte">
                <font/>
                <edge COLOR="#000000"/>
                <node ID="ID_136" POSITION="left" TEXT="Te gusta la rumba">
                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_137" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                        <font/>
                        <edge COLOR="#000000"/>
                        <node ID="ID_216" POSITION="left" TEXT="Para bailar">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_224" POSITION="left" STYLE="bubble" TEXT="Mango's">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_226" POSITION="left" STYLE="bubble" TEXT="Kukaramakara">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_235" POSITION="left" STYLE="bubble" TEXT="El Pub">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_233" POSITION="left" STYLE="bubble" TEXT="B Lounge">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_234" POSITION="left" STYLE="bubble" TEXT="Mamma Juana">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                        </node>
                        <node ID="ID_217" POSITION="left" TEXT="Para o&#237;r m&#250;sica">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_225" POSITION="left" STYLE="bubble" TEXT="Palmah&#237;a">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_236" POSITION="left" STYLE="bubble" TEXT="El Deck">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_232" POSITION="left" STYLE="bubble" TEXT="Hard Rock Caf&#233;">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                        </node>
                        <node ID="ID_222" POSITION="left" TEXT="&#191;Y para tocar m&#250;sica?">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_223" POSITION="left" STYLE="bubble" TEXT="Red de Escuelas de M&#250;sica">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                        </node>
                    </node>
                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_173" POSITION="left" STYLE="bubble" TEXT="No">
                        <font/>
                        <edge COLOR="#000000"/>
                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_174" POSITION="left" STYLE="bubble" TEXT="Hacer deporte s&#237;">
                            <font/>
                            <edge COLOR="#000000"/>
                            <node ID="ID_208" POSITION="left" TEXT="Al aire libre">
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_213" POSITION="left" STYLE="bubble" TEXT="Los domingos y festivos en la ciclov&#237;a">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_214" POSITION="left" STYLE="bubble" TEXT="En caminatas por los parques">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                            </node>
                            <node ID="ID_209" POSITION="left" TEXT="En un centro">
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_212" POSITION="left" STYLE="bubble" TEXT="Estadio Atanasio Girardot">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_215" POSITION="left" STYLE="bubble" TEXT="Unidad Deportiva Atanasio Girardot">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_211" POSITION="left" STYLE="bubble" TEXT="Unidades Deportivas INDER">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_210" POSITION="left" STYLE="bubble" TEXT="Polideportivo UPB">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                            </node>
                        </node>
                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_138" POSITION="left" STYLE="bubble" TEXT="Algo m&#225;s calmado">
                            <font/>
                            <edge COLOR="#000000"/>
                            <node ID="ID_139" POSITION="left" TEXT="&#191;Salir de compras?">
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_141" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                    <node ID="ID_143" POSITION="left" TEXT="&#191;Traes dinero en el bolsillo?">
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_145" POSITION="left" STYLE="bubble" TEXT="No">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                            <node ID="ID_146" POSITION="left" TEXT="&#191;D&#243;nde sacar dinero?">
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_147" POSITION="left" STYLE="bubble" TEXT="En Cajeros Autom&#225;ticos">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_148" POSITION="left" STYLE="bubble" TEXT="En un Banco">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_149" POSITION="left" STYLE="bubble" TEXT="Bancolombia">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_150" POSITION="left" STYLE="bubble" TEXT="Banco Santander">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_151" POSITION="left" STYLE="bubble" TEXT="Banco BBVA">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_152" POSITION="left" STYLE="bubble" TEXT="Banco Caja Social">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_153" POSITION="left" STYLE="bubble" TEXT="Banco AV Villas">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_154" POSITION="left" STYLE="bubble" TEXT="Banco de Bogot&#225;">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_155" POSITION="left" STYLE="bubble" TEXT="Banco Popular">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_156" POSITION="left" STYLE="bubble" TEXT="Davivienda">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_157" POSITION="left" STYLE="bubble" TEXT="Otros">
                                                        <font/>
                                                        <edge COLOR="#000000"/>
                                                    </node>
                                                </node>
                                            </node>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_144" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                            <node ID="ID_158" POSITION="left" TEXT="En mediana cantidad">
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_160" POSITION="left" STYLE="bubble" TEXT="Centro de la Moda (Itagu&#237;)">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_161" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Boulevar El Hueco">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_162" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Jap&#243;n">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_163" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Hollywood">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_167" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Palacio Nacional">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                            </node>
                                            <node ID="ID_159" POSITION="left" TEXT="En gran cantidad">
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_164" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Punto Clave">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_165" POSITION="left" STYLE="bubble" TEXT="Centro Comercial El Tesoro">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_166" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Santaf&#233;">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_168" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Monterrey">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_169" POSITION="left" STYLE="bubble" TEXT="Centro Comercial San Diego">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_170" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Oviedo">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_171" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Unicentro">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_172" POSITION="left" STYLE="bubble" TEXT="Centro Comercial Mayorca">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                            </node>
                                        </node>
                                    </node>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_142" POSITION="left" STYLE="bubble" TEXT="No">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                    <node ID="ID_175" POSITION="left" TEXT="&#191;Ir a comer algo?">
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_176" POSITION="left" STYLE="bubble" TEXT="Un caf&#233; o algo as&#237;">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_178" POSITION="left" STYLE="bubble" TEXT="Versalles">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_179" POSITION="left" STYLE="bubble" TEXT="El Astor">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_177" POSITION="left" STYLE="bubble" TEXT="Un buen almuerzo">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_180" POSITION="left" STYLE="bubble" TEXT="San Carb&#243;n">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_181" POSITION="left" STYLE="bubble" TEXT="Hatoviejo">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_182" POSITION="left" STYLE="bubble" TEXT="Triada">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_183" POSITION="left" STYLE="bubble" TEXT="Il Forno">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_227" POSITION="left" STYLE="bubble" TEXT="Mondongo's">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                        </node>
                                    </node>
                                </node>
                            </node>
                            <node ID="ID_140" POSITION="left" TEXT="&#191;Algo educativo y divertido?">
                                <node ID="ID_184" POSITION="left" TEXT="&#191;Parques?">
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_186" POSITION="left" STYLE="bubble" TEXT="Educativos">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_188" POSITION="left" STYLE="bubble" TEXT="Parque Explora">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_194" POSITION="left" STYLE="bubble" TEXT="Museo Interactivo EPM">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node ID="ID_189" POSITION="left" TEXT="Conocer m&#225;s centros educativos">
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_198" POSITION="left" STYLE="bubble" TEXT="Universidad de Antioquia">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_199" POSITION="left" STYLE="bubble" TEXT="Universidad Nacional">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_200" POSITION="left" STYLE="bubble" TEXT="Universidad de Medell&#237;n">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_201" POSITION="left" STYLE="bubble" TEXT="Universidad EAFIT">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_202" POSITION="left" STYLE="bubble" TEXT="Universidad Pontificia Bolivariana">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_203" POSITION="left" STYLE="bubble" TEXT="Fundaci&#243;n Universitaria Luis Amig&#243;">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_204" POSITION="left" STYLE="bubble" TEXT="Universidad Santo Tom&#225;s">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_205" POSITION="left" STYLE="bubble" TEXT="Universidad CES">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                        </node>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_187" POSITION="left" STYLE="bubble" TEXT="Divertidos">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_195" POSITION="left" STYLE="bubble" TEXT="Parque de Las Aguas">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_196" POSITION="left" STYLE="bubble" TEXT="Parque Norte">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_197" POSITION="left" STYLE="bubble" TEXT="Aeroparque Juan Pablo II">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                    </node>
                                </node>
                                <node ID="ID_185" POSITION="left" TEXT="&#191;Otros centros?">
                                    <node BACKGROUND_COLOR="#000000" COLOR="#f2f2f2" ID="ID_190" POSITION="left" STYLE="bubble" TEXT="El Planetario de Medell&#237;n">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#f2f2f2" ID="ID_191" POSITION="left" STYLE="bubble" TEXT="La Plaza de Toros La Macarena">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_192" POSITION="left" STYLE="bubble" TEXT="Jard&#237;n Bot&#225;nico Joaqu&#237;n Antonio Uribe">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_193" POSITION="left" STYLE="bubble" TEXT="Zool&#243;gico de Santa Fe">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
            </node>
            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_5" POSITION="left" STYLE="bubble" TEXT="Conocer m&#225;s de tu ciudad">
                <font/>
                <edge COLOR="#000000"/>
                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_7" POSITION="left" STYLE="bubble" TEXT="De la historia">
                    <font/>
                    <edge COLOR="#000000"/>
                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_10" POSITION="left" STYLE="bubble" TEXT="De una manera divertida">
                        <font/>
                        <edge COLOR="#000000"/>
                        <node ID="ID_12" POSITION="left" TEXT="&#191;Te gustan los teatros?">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_14" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node ID="ID_15" POSITION="left" TEXT="&#191;Los Cl&#225;sicos?">
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_16" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node ID="ID_21" POSITION="left" TEXT="Entonces puedes visitar">
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_19" POSITION="left" STYLE="bubble" TEXT="Teatro Pablo Tob&#243;n Uribe">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_18" POSITION="left" STYLE="bubble" TEXT="Teatro Lido">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_20" POSITION="left" STYLE="bubble" TEXT="Teatro Metropolitano">
                                                <font ITALIC="true"/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_22" POSITION="left" STYLE="bubble" TEXT="Teatro Porfirio Barba Jacob">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                        </node>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_17" POSITION="left" STYLE="bubble" TEXT="No">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node ID="ID_23" POSITION="left" TEXT="Seguro te gustar&#225;n estos">
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_24" POSITION="left" STYLE="bubble" TEXT="El Peque&#65533;&#65533;o Teatro">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_25" POSITION="left" STYLE="bubble" TEXT="Teatro de Mu&#241;ecos La Fanfarria">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_26" POSITION="left" STYLE="bubble" TEXT="Teatro El &#193;guila Descalza">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_27" POSITION="left" STYLE="bubble" TEXT="Teatro Manicomio De Mu&#65533;&#65533;ecos">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_28" POSITION="left" STYLE="bubble" TEXT="Teatro Matacandelas">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_29" POSITION="left" STYLE="bubble" TEXT="Teatro Universidad de Medell&#237;n">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_30" POSITION="left" STYLE="bubble" TEXT="Teatro Caja Negra">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                        </node>
                                    </node>
                                </node>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_13" POSITION="left" STYLE="bubble" TEXT="No">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node ID="ID_31" POSITION="left" TEXT="&#191;Qu&#233; tal los museos?">
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_33" POSITION="left" STYLE="bubble" TEXT="Est&#225;n bien">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node ID="ID_34" POSITION="left" TEXT="&#191;De los antiguos?">
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_35" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_37" POSITION="left" STYLE="bubble" TEXT="Museo de Antioquia">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_38" POSITION="left" STYLE="bubble" TEXT="Museo Cementerio San Pedro">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_41" POSITION="left" STYLE="bubble" TEXT="Museo El Castillo">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_36" POSITION="left" STYLE="bubble" TEXT="Algo m&#225;s nuevo, mejor">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_39" POSITION="left" STYLE="bubble" TEXT="Museo de Arte Moderno (MAMM)">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_40" POSITION="left" STYLE="bubble" TEXT="Museo Universitario Universidad de Antioquia (MUUA)">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_42" POSITION="left" STYLE="bubble" TEXT="Casa Museo Maestro Pedro Nel G&#243;mez">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_43" POSITION="left" STYLE="bubble" TEXT="Casa Museo Gardeliana ">
                                                    <font/>
                                                    <edge COLOR="#000000"/>
                                                </node>
                                            </node>
                                        </node>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_32" POSITION="left" STYLE="bubble" TEXT="No te gustan">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_11" POSITION="left" STYLE="bubble" TEXT="De la historia que est&#225; en los libros">
                        <font/>
                        <edge COLOR="#000000"/>
                        <node ID="ID_44" POSITION="left" TEXT="&#191;Quieres ir a una biblioteca?">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_45" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_47" POSITION="left" STYLE="bubble" TEXT="De las tradicionales">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_49" POSITION="left" STYLE="bubble" TEXT="Biblioteca P&#250;blica Piloto">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_50" POSITION="left" STYLE="bubble" TEXT="Biblioteca EPM">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_48" POSITION="left" STYLE="bubble" TEXT="Parques biblioteca">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_52" POSITION="left" STYLE="bubble" TEXT="Espa&#241;a">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_51" POSITION="left" STYLE="bubble" TEXT="San Javier">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_55" POSITION="left" STYLE="bubble" TEXT="La Quintana">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_53" POSITION="left" STYLE="bubble" TEXT="La Ladera">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_54" POSITION="left" STYLE="bubble" TEXT="Bel&#233;n">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                </node>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_46" POSITION="left" STYLE="bubble" TEXT="No">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node ID="ID_65" POSITION="left" TEXT="&#191;Algo m&#225;s de mapas?">
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_66" POSITION="left" STYLE="bubble" TEXT="S&#237;, de historia de verdad">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_68" POSITION="left" STYLE="bubble" TEXT="Archivo Hist&#243;rico de Medell&#237;n">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_69" POSITION="left" STYLE="bubble" TEXT="Academia de Historia de Antioquia">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_67" POSITION="left" STYLE="bubble" TEXT="No">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node ID="ID_61" POSITION="left" TEXT="&#191;Mejor a un parque?">
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_62" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                                <arrowlink DESTINATION="ID_76" ENDARROW="Default"/>
                                            </node>
                                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_63" POSITION="left" STYLE="bubble" TEXT="Tampoco">
                                                <font/>
                                                <edge COLOR="#000000"/>
                                            </node>
                                        </node>
                                    </node>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_9" POSITION="left" STYLE="bubble" TEXT="De la geograf&#237;a">
                    <font/>
                    <edge COLOR="#000000"/>
                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_70" POSITION="left" STYLE="bubble" TEXT="Parques naturales">
                        <font/>
                        <edge COLOR="#000000"/>
                        <node BACKGROUND_COLOR="#000000" COLOR="#f5f5f5" ID="ID_72" POSITION="left" STYLE="bubble" TEXT="Parque ecol&#243;gico Piedras Blancas">
                            <font/>
                            <edge COLOR="#000000"/>
                        </node>
                        <node BACKGROUND_COLOR="#000000" COLOR="#f5f5f5" ID="ID_73" POSITION="left" STYLE="bubble" TEXT="Ecoparque Cerro El Volador">
                            <font/>
                            <edge COLOR="#000000"/>
                        </node>
                        <node BACKGROUND_COLOR="#000000" COLOR="#f5f5f5" ID="ID_74" POSITION="left" STYLE="bubble" TEXT="Parque Ecol&#243;gico Cerro Nutibara">
                            <font/>
                            <edge COLOR="#000000"/>
                        </node>
                        <node BACKGROUND_COLOR="#000000" COLOR="#f5f5f5" ID="ID_75" POSITION="left" STYLE="bubble" TEXT="Parque Ecotur&#237;stico Arv&#237;">
                            <font/>
                            <edge COLOR="#000000"/>
                        </node>
                    </node>
                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_71" POSITION="left" STYLE="bubble" TEXT="Parques urbanos">
                        <font/>
                        <edge COLOR="#000000"/>
                        <node ID="ID_76" POSITION="left" TEXT="&#191;De los tradicionales?">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_79" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node BACKGROUND_COLOR="#000000" COLOR="#f7f7f7" ID="ID_77" POSITION="left" STYLE="bubble" TEXT="Plazas">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_81" POSITION="left" STYLE="bubble" TEXT="Plaza de San Antonio">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_82" POSITION="left" STYLE="bubble" TEXT="Plaza Botero">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_83" POSITION="left" STYLE="bubble" TEXT="Plaza Cisneros">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_85" POSITION="left" STYLE="bubble" TEXT="Plazuela San Ignacio">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_84" POSITION="left" STYLE="bubble" TEXT="Plaza de la Libertad">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_86" POSITION="left" STYLE="bubble" TEXT="Plazuela Nutibara">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_87" POSITION="left" STYLE="bubble" TEXT="Plazuela de la Veracruz">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#f7f7f7" ID="ID_78" POSITION="left" STYLE="bubble" TEXT="Parques">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_88" POSITION="left" STYLE="bubble" TEXT="Parque de Bol&#237;var">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_89" POSITION="left" STYLE="bubble" TEXT="Parque de Berr&#237;o">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_90" POSITION="left" STYLE="bubble" TEXT="Parque de Boston">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_91" POSITION="left" STYLE="bubble" TEXT="Parque del Poblado">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_92" POSITION="left" STYLE="bubble" TEXT="Parque de Bel&#233;n">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_93" POSITION="left" STYLE="bubble" TEXT="Parque del Periodista">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                </node>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_80" POSITION="left" STYLE="bubble" TEXT="No, algo distinto">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_94" POSITION="left" STYLE="bubble" TEXT="Parque Lleras">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_95" POSITION="left" STYLE="bubble" TEXT="Parque de los Pies Descalzos">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_96" POSITION="left" STYLE="bubble" TEXT="Parque Lineal La Presidenta">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_97" POSITION="left" STYLE="bubble" TEXT="Parque de Los Deseos">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_98" POSITION="left" STYLE="bubble" TEXT="Parque de La Bailarina">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_99" POSITION="left" STYLE="bubble" TEXT="Parque Juanes de La Paz">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                            </node>
                        </node>
                    </node>
                </node>
            </node>
            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_8" POSITION="left" STYLE="bubble" TEXT="Otra informaci&#243;n">
                <font/>
                <edge COLOR="#000000"/>
                <node ID="ID_102" POSITION="left" TEXT="&#191;Una emergencia?">
                    <node ID="ID_105" POSITION="left" TEXT="S&#237;">
                        <node ID="ID_107" POSITION="left" TEXT="&#191;Tienes tel&#233;fono?">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_108" POSITION="left" STYLE="bubble" TEXT="S&#237;">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_117" POSITION="left" STYLE="bubble" TEXT="Polic&#237;a: Llama al 112">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_119" POSITION="left" STYLE="bubble" TEXT="Seguridad: Llama al 123">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_118" POSITION="left" STYLE="bubble" TEXT="Salud: Llama al 125">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                                <node BACKGROUND_COLOR="#ff0000" COLOR="#fcfcfc" ID="ID_120" POSITION="left" STYLE="bubble" TEXT="Informaci&#243;n: ">
                                    <font/>
                                    <edge COLOR="#000000"/>
                                </node>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_109" POSITION="left" STYLE="bubble" TEXT="No, es mejor ir">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node ID="ID_100" POSITION="left" TEXT="&#191;Alg&#250;n punto de Salud?">
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_101" POSITION="left" STYLE="bubble" TEXT="Hospitales">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_121" POSITION="left" STYLE="bubble" TEXT="Hospital Universitario San Vicente de Pa&#250;l">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_122" POSITION="left" STYLE="bubble" TEXT="Hospital Pablo Tob&#243;n Uribe">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_123" POSITION="left" STYLE="bubble" TEXT="Hospital General de Medell&#237;n">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_104" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nicas">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_124" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica Soma">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_125" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica Medell&#237;n">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_126" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica CES">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_127" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica Las Am&#233;ricas">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_131" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica Cardiovascular">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_128" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica Las Vegas">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_129" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica El Rosario">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_130" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica El Prado">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_132" POSITION="left" STYLE="bubble" TEXT="Cl&#237;nica El Sagrado Coraz&#243;n">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                    </node>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node ID="ID_106" POSITION="left" TEXT="No">
                        <node ID="ID_103" POSITION="left" TEXT="&#191;Est&#225;s perdido?">
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_110" POSITION="left" STYLE="bubble" TEXT="Un poco">
                                <font/>
                                <edge COLOR="#000000"/>
                                <node ID="ID_112" POSITION="left" TEXT="Puedes utilizar">
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_113" POSITION="left" STYLE="bubble" TEXT="Un bus">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_114" POSITION="left" STYLE="bubble" TEXT="El Metro">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                        <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_116" POSITION="left" STYLE="bubble" TEXT="Metro Cable">
                                            <font/>
                                            <edge COLOR="#000000"/>
                                        </node>
                                    </node>
                                    <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_115" POSITION="left" STYLE="bubble" TEXT="El Metropl&#250;s">
                                        <font/>
                                        <edge COLOR="#000000"/>
                                    </node>
                                </node>
                            </node>
                            <node BACKGROUND_COLOR="#000000" COLOR="#ffffff" ID="ID_111" POSITION="left" STYLE="bubble" TEXT="No">
                                <font/>
                                <edge COLOR="#000000"/>
                            </node>
                        </node>
                    </node>
                </node>
            </node>
        </node>
    </node>
</map>